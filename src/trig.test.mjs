import test from 'ava'
import {angle_between} from './trig.mjs'

test('angle_between', t=>{
    t.truthy(angle_between(0, 1, 1.2))
    t.falsy(angle_between(-1, -1.1, 0.1))
    // TODO josh: improve angle_between to be more generically correct
    t.truthy(angle_between(0-2*Math.PI, 1, 1.2-2*Math.PI))
})
