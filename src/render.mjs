import Beep, {beep_vision} from './type/Beep.mjs'
import UI from './type/UI.mjs'
import Thing from './type/Thing.mjs'

export function draw_beep(ctx, beep, x, y){
    let size = beep.get(Beep.size)
    ctx.fillStyle = beep_color(beep)
    let radius = size/2
    ctx.beginPath()
    ctx.arc(x, y, size/2, 0, Math.PI*2)
    ctx.closePath()
    ctx.fill()
    let reach_radius = Math.hypot(radius, radius)
    let shield = beep.get(Beep.shield)
    if (shield)
    {
        ctx.arc(x, y, reach_radius, 0, (shield/100)*2*Math.PI)
        ctx.lineWidth = 3
        ctx.strokeStyle = 'black'
        ctx.stroke()
    }
    {
        ctx.fillStyle = 'rgba(255, 192, 203, 0.25)'
        ctx.beginPath()
        ctx.moveTo(x, y)
        let vision = beep_vision(beep)
        ctx.arc(x, y, vision.distance, vision.angle_left, vision.angle_right)
        ctx.moveTo(x, y)
        ctx.closePath()
        ctx.fill()
    }
    if (beep.has(UI.tracking))
    {
        ctx.beginPath()
        ctx.fillStyle = 'black'
        ctx.arc(x, y, 5, 0, Math.PI*2)
        ctx.closePath()
        ctx.fill()
    }
}

export function draw_grass(ctx, grass, x, y){
    ctx.beginPath()
    ctx.fillStyle = 'green'
    let energy = grass.get(Thing.energy)
    let radius = Math.max(1, Math.log(energy))
    ctx.arc(x, y, radius, 0, 2*Math.PI)
    ctx.fill()
    ctx.closePath()
    return radius
}

export function draw_dead(ctx, x, y){
    ctx.fillStyle = 'black'
    ctx.fillText('DEAD', x, y)
}

export function draw_unknown(ctx, x, y){
    ctx.fillStyle = 'black'
    ctx.font = '16px sans'
    ctx.fillText('???', x-10, y)
}

export function draw_target(ctx, radius, box){
    let center = {x: box.width/2, y: box.height/2}
    ctx.strokeStyle = 'black'
    ctx.beginPath()
    ctx.arc(center.x, center.y, radius, 0, Math.PI*2)
    ctx.moveTo(center.x, center.y-radius)
    ctx.lineTo(center.x, 0)
    ctx.moveTo(center.x, center.y+radius)
    ctx.lineTo(center.x, box.height)
    ctx.moveTo(center.x-radius, center.y)
    ctx.lineTo(0, center.y)
    ctx.moveTo(center.x+radius, center.y)
    ctx.lineTo(box.width, center.y)
    ctx.closePath()
    ctx.stroke()
}

function beep_color(e){
    let energy = e.get(Thing.energy)
    let size = e.get(Beep.size)
    let starting = (()=>{
        let goal = e.get(Beep.goal)
        switch (goal)
        {
        case 'shield': return [40, 40, 40]
        case 'wander': return [0, 0, 255]
        case 'grow': return [0, 255, 0]
        case 'idle': return [255, 255, 0]
        case 'hunt': return [255, 0, 0]
        case 'flee': return [255, 255, 0]
        }
        throw new Error(`Beep.color unreachable (goal=${goal})`)
    })()
    let channels = starting.map(v=>Math.min(255, Math.round(v*(energy/size))))
    return `rgba(${channels.join(', ')})`
}
