export class Default_map extends Map {
    constructor(builder){
        super()
        this.default = builder
    }
    get(k){
        if (!this.has(k))
            this.set(k, this.default())
        return super.get(k)
    }
}

export class Counter {
    constructor(){ this.value = 0 }
    inc(){ return ++this.value }
}

export function assert(expression, message){
    if (!expression)
        throw new Error(message)
}

export function random(a, b){ return (Math.random()*(b-a))+a }
