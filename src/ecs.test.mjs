import test from 'ava'
import ecs from './ecs.mjs'

test('lookups', t=>{
    let world = ecs.world()
    let position = world.component('position')
    let player = world.component('player')
    let player1 = world.entity()
        .add(player, true)
        .add(position, {x: 10, y: 10})
    t.is(player1.get(player), true)
})

test('query', t=>{
    let world = ecs.world()
    let position = world.component('position')
    let c_player = world.component('player')
    let player = world.entity()
        .add(c_player)
        .add(position, {x: 10, y: 10})
    let tree = world.component('player')
    let growth = world.component('growth')
    let trees = []
    for (let i = 0; i<10; i++)
    {
        trees.push(world.entity()
            .add(tree)
            .add(position, {x: 100+i, y: 100+i})
            .add(growth, 0.2))
    }
    let equal_ids = (a, b)=>t.deepEqual(a.map(x=>x.id), b.map(x=>x.id))
    equal_ids(world.query([position]), [player, ...trees])
    equal_ids(world.query([position, tree]), trees)
})
