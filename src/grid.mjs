import {assert} from './util.mjs'

class Grid {
    constructor({min_x, max_x, min_y, max_y, bucket_size}){
        this.bucket_size = bucket_size
        this.boxes = new Map()
        this.buckets = new Map()
        for (let i = min_x; i<max_x; i+=bucket_size)
        {
            for (let ii = min_y; ii<max_y; ii+=bucket_size)
            {
                this.make_bucket(i, ii)
            }
        }
    }
    *cells(){
        for (let [key, c] of this.buckets)
            yield [this.boxes.get(key), c]
    }
    key(x, y){
        let round = this.round
        return round(x)+'-'+round(y)
    }
    add(x, y, v){
        assert(!v.DEAD, 'Not allowed to add dead things to the grid')
        let bucket_id = this.key(x, y)
        v.event(`Added to grid bucket ${bucket_id}`)
        if (!this.buckets.has(bucket_id))
            this.make_bucket(x, y)
        this.buckets.get(bucket_id).add(v)
    }
    delete(x, y, v){
        let bucket_id = this.key(x, y)
        let bucket = this.buckets.get(bucket_id)
        assert(bucket.has(v), `Can't drop entity from bucket ${bucket_id}, already not there`)
        v.event(`Dropped from grid bucket ${bucket_id}`)
        bucket.delete(v)
    }
    lookup(x, y){
        // TODO josh: allow passing a bounding box and returning values from
        // all matching cells
        return this.buckets.get(this.key(x, y)).values()
    }
    // XXX lou: I might have assumed untruths, but it seems to work correctly
    make_bucket(x, y){
        let b = this.bucket_size
        let round = this.round
        let key = this.key(x, y)
        this.boxes.set(key, {
            x: round(x)*b,
            x2: round(x)*b+b,
            y: round(y)*b,
            y2: round(y)*b+b,
        })
        this.buckets.set(key, new Set())
    }
    round = v=>Math.floor(v/this.bucket_size)
}

export default Grid
