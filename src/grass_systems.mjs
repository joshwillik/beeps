import {random} from './util.mjs'
import Thing from './type/Thing.mjs'
import Grass from './type/Grass.mjs'
import {add_beep} from './type/Beep.mjs'

export function grass_spawns({world, dt, grid}){
    let spawn_chance = 0.06*dt
    if (Math.random()<spawn_chance)
    {
        let {stage} = world.context
        let x = random(0, stage.x)
        let y = random(0, stage.y)
        let entity = world.entity()
            .add(Grass.type)
            .add(Thing.position, {x, y})
            .add(Thing.energy, 0)
        // TODO josh: auto-register entities with the grid so this doesn't need
        // to be manually managed
        grid.add(x, y, entity)
    }
}

export function grass_grows({world, dt}){
    for (let e of world.query([Grass.type]))
        e.update(Thing.energy, v=>v+dt)
}

export function grass_becomes_beeps({world, dt, grid}){
    for (let e of world.query([Grass.type]))
    {
        let {x, y} = e.get(Thing.position)
        let energy = e.get(Thing.energy)
        let convert_chance = (1/500_000)*dt
        if (energy<=100 || Math.random()>=convert_chance)
            continue
        world.remove(e)
        let new_beep = add_beep(world, {x, y, energy: energy/2})
        grid.add(x, y, new_beep)
    }
}
