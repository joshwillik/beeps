function filter(set, fn){
    for (let x of set)
        if (!fn(x))
            set.delete(x)
    return set
}

export default {filter}
