// TODO:
// - v1: grass has variable growth based on cloud cover
// - v2: sunlight
// - v3: wind
// - beeps need to flee hunters
// - beeps need to have genetics
// - beeps need to reproduce
// - beeps need to have some ways of working together
import Event_emitter from 'events'
import {createElement as el, useContext, useEffect,
    useState} from 'react'
import {createRoot} from 'react-dom/client'
import {throttle} from 'lodash-es'
import ecs from './ecs.mjs'
import {random, Default_map, Counter} from './util.mjs'
import Beep, {beep_can_see, beep_can_reach, add_beep, pick_scary, pick_huntable,
    pick_wander} from './type/Beep.mjs'
import Grass from './type/Grass.mjs'
import UI, {focus_thing, unfocus} from './type/UI.mjs'
import Thing from './type/Thing.mjs'
import Wind from './type/Wind.mjs'
import Cloud from './type/Cloud.mjs'
import {beeps_see, beeps_fight, beeps_starve,
    beeps_bounce_off_walls, beeps_shield, beeps_grow} from './beep_systems.mjs'
import {wind_on_clouds, clouds_despawn} from './cloud_systems.mjs'
import {grass_spawns, grass_grows, grass_becomes_beeps} from './grass_systems.mjs'
import {FPS} from './metrics.mjs'
import Event_bus_context from './overlay/Event_bus_context.mjs'
import World_context from './overlay/World_context.mjs'
import Overlay from './overlay/Overlay.mjs'
import {draw_beep, draw_target, draw_grass} from './render.mjs'
import Grid from './grid.mjs'
import {distance_between} from './trig.mjs'

let {max, log, PI} = Math
let world = window.world = ecs.world({
    stage: {
        x: 0,
        y: 0,
        bounding_box(){
            return {
                x: 0,
                x2: this.x,
                y: 0,
                y2: this.y,
            }
        },
    },
    viewport: {width: 0, height: 0},
    settings: {
        camera_pan_speed: 10,
        camera_zoom_speed: 0.0015,
        mouse_target_distance: 10,
    },
    camera: {x: 0, y: 0, zoom: 1},
    controls: {},
})
let SYSTEMS = [
    wind_on_clouds,
    beeps_see,
    beeps_decide,
    things_move,
    move_camera,
    beeps_bounce_off_walls,
    beeps_shield,
    beeps_grow,
    clouds_despawn,
    //grass_spawns,
    grass_grows,
    grass_becomes_beeps,
    beeps_fight,
    beeps_starve,
    drop_from_grid,
]

let beep_logic = (()=>{
    // Each state has a logic function.
    // States are biased towards finishing their current action (e.g. moving,
    // shielding), but can return a new goal function to transition the beep to
    // a new state of mind
    let states = {}
    states.flee = (_, e)=>{
        let other = e.get(Beep.fleeing)
        /*let flee_way = unit_vector({
            x: other_pos.x + beep_pos.x,
            y: other_pos.y + beep_pos.y,
        })
        e.add(Thing.movement, flee_way)*/
        console.log('Something is fleeing.')
        return states.idle
    }
    states.shield = (_, e)=>{
        let energy = e.get(Thing.energy)
        if (energy>e.get(Beep.size))
            return states.idle
    }

    states.wander = (_, e, debug)=>{
        let wander_target = debug.wander_target = e.get(Beep.wander_to)
        let target_distance = debug.target_distance =
            distance_between(e.get(Thing.position), wander_target)
        if (target_distance<1)
        {
            e.remove(Beep.wander_to)
            return states.idle
        }
        let scary = pick_scary(e)
        // TODO lou: fix infinite loop before re-enabling
        if (0 && scary)
        {
            e.add(Beep.fleeing, scary)
            e.remove(Beep.wander_to)
            return states.flee
        }
        let huntable = pick_huntable(e)
        if (huntable)
        {
            e.add(Beep.hunting, huntable)
            e.remove(Beep.wander_to)
            return states.hunt
        }
        let pos = debug.position = e.get(Thing.position)
        let wander_ray = unit_vector({
            x: wander_target.x - pos.x,
            y: wander_target.y - pos.y,
        })
        e.add(Thing.movement, wander_ray)
    }

    states.hunt = (_, e)=>{
        let hunting = e.get(Beep.hunting)
        let hunting_pos = hunting?.get(Thing.position)
        let huntable = pick_huntable(e)
        if (!huntable)
        {
            e.remove(Beep.hunting)
            return states.idle
        }
        e.add(Beep.hunting, huntable)
        let pos = e.get(Thing.position)
        let other_pos = huntable.get(Thing.position)
        // TOOD josh: make this respect a beep's max speed
        let hunt_ray = unit_vector({
            x: other_pos.x - pos.x,
            y: other_pos.y - pos.y,
        })
        e.add(Thing.movement, hunt_ray)
    }

    states.idle = (_, e, debug)=>{
        let scary = pick_scary(e)
        // TODO lou: fix infinite loop before re-enabling
        if (0 && scary)
        {
            e.add(Beep.fleeing, scary)
            return states.flee
        }
        let huntable = pick_huntable(e)
        if (huntable)
        {
            e.add(Beep.hunting, huntable)
            return states.hunt
        }
        let wander_target = debug.wander_target = pick_wander(e)
        debug.wander_distance = distance_between(e.get(Thing.position), wander_target)
        debug.size = e.get(Beep.size)
        e.add(Beep.wander_to, wander_target)
        return states.wander
    }

    for (let [id, state_fn] of Object.entries(states))
        state_fn.state_id = id

    return (context, entity, goal)=>{
        let {now} = context
        let state_counter = new Default_map(()=>new Counter())
        let state_log = []
        let state_fn = states[goal]
        let new_goal
        while (state_fn)
        {
            let debug = {}
            state_log.push(debug)
            if (state_counter.get(state_fn).inc()>2)
                throw new Error(`Entered ${state_fn.state_id} too many times in 1 frame`)
            state_fn = state_fn(context, entity, debug)
            if (state_fn)
            {
                new_goal = state_fn.state_id
                entity.add(Beep.decision, now)
            }
        }
        if (new_goal)
            entity.add(Beep.goal, new_goal)
    }
})()

function beeps_decide(context){
    let {world} = context
    for (let entity of world.query([Beep.type]))
        beep_logic(context, entity, entity.get(Beep.goal))
}

function move_camera({world, dt, grid}){
    let {settings, camera, controls} = world.context
    let [focused] = world.query([UI.focused])
    if (focused)
    {
        let skew = focused_skew(world)
        camera.x = skew.x
        camera.y = skew.y
        return
    }
    if (controls.pan_left)
        camera.x += settings.camera_pan_speed
    if (controls.pan_right)
        camera.x -= settings.camera_pan_speed
    if (controls.pan_up)
        camera.y += settings.camera_pan_speed
    if (controls.pan_down)
        camera.y -= settings.camera_pan_speed
}

function things_move({world, dt, grid}){
    for (let entity of world.query([Thing.position, Thing.movement]))
    {
        let {x, y} = entity.get(Thing.position)
        let {x: dx, y: dy} = entity.get(Thing.movement)
        let {x: x2, y: y2} = entity.update(Thing.position, pos=>{
            return {
                x: pos.x + dx*dt,
                y: pos.y + dy*dt,
            }
        })
        if (grid.key(x, y)!=grid.key(x2, y2))
        {
            grid.delete(x, y, entity)
            grid.add(x2, y2, entity)
        }
        if (entity.has(Beep.type)) // beeps need to spend calories to move
        {
            let energy_cost = entity.get(Beep.size)**2*0.0001
            entity.update(Thing.energy, v=>v - energy_cost*dt)
        }
    }
}

function Game_overlay({world}){
    let event_bus = useContext(Event_bus_context)
    let [tracking, set_tracking] = useState(()=>[])
    useEffect(()=>{
        let update_tracking = ()=>{
            set_tracking(world.query([UI.tracking]))
        }
        let sidebar_sync = setInterval(update_tracking, 1_000)
        event_bus.on('ui.click', update_tracking)
        return ()=>{
            clearInterval(sidebar_sync)
            event_bus.off('ui.click', update_tracking)
        }
    }, [world, event_bus, set_tracking])
    return el(World_context.Provider, {value: world}, el(Overlay))
}

export function start_game(box){
    let {stage, viewport, settings} = world.context
    box.style.position = 'relative'
    let canvas = document.createElement('canvas')
    box.appendChild(canvas)
    function on_resize(){
        viewport.width = canvas.width = window.innerWidth
        viewport.height = canvas.height = window.innerHeight
    }
    on_resize()
    window.addEventListener('resize', throttle(on_resize, 100))
    let event_bus = new Event_emitter()
    let ui_div = document.createElement('div')
    Object.assign(ui_div.style, {
        position: 'absolute',
        zIndex: 10,
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
    })
    box.appendChild(ui_div)
    let ui_root = createRoot(ui_div)
    ui_root.render(el(
        Event_bus_context.Provider,
        {value: event_bus},
        el(Game_overlay, {world}),
    ))
    let ctx = canvas.getContext('2d', {alpha: false})
    stage.x = ctx.canvas.width
    stage.y = ctx.canvas.height
    let buffer = 100
    let grid = new Grid({
        min_x: -buffer,
        max_x: stage.x+buffer,
        min_y: -buffer,
        max_y: stage.y+buffer,
        bucket_size: 100,
    })
    world.entity()
        .add(Wind.force, {x: random(-0.2, 0.2), y: random(-0.2, 0.2)})
    // eslint-disable-next-line no-unused-vars
    for (let _ of range(0))
    {
        let x = random(0, stage.x)
        let y = random(0, stage.y)
        let cloud = world.entity()
            .add(Cloud.type, {size: random(100, 500), z: random(1, 10)})
            .add(Thing.movement, {x: 0, y: 0})
            .add(Thing.position, {x, y})
        grid.add(x, y, cloud)
    }
    // eslint-disable-next-line no-unused-vars
    for (let _ of range(1))
    {
        let beep = add_beep(world)
        let {x, y} = beep.get(Thing.position)
        grid.add(x, y, beep)
    }
    world.context.time_flow = 1
    let last_mouse = Date.now()
    document.addEventListener('mousemove', ()=>last_mouse = Date.now())
    let fps = new FPS()
    let now = 0
    let tick = ()=>{
        if (stage.width < 1920 && Date.now()-last_mouse > 30_000)
        {
            // This is just so my laptop battery doesn't drain while I code
            return setTimeout(tick, 1_000)
        }
        let dt = world.context.time_flow
        now += dt
        let context = {
            world,
            grid,
            fps,
            dt,
            now,
            frame_state: {},
        }
        apply_systems(context)
        render(ctx, context)
        fps.inc()
        event_bus.emit('canvas.render')
        requestAnimationFrame(tick)
    }
    user_input({canvas, world, grid, settings, event_bus})
    tick()
}

let user_input = ({world, grid, settings, event_bus})=>{
    let {controls, camera} = world.context
    document.addEventListener('keydown', e=>{
        if (e.key=='i')
            return world.context.debug = !world.context.debug
        if (e.key==' ')
            return world.context.time_flow = world.context.time_flow ? 0 : 1
        if (e.key=='a'||e.key=='ArrowLeft')
        {
            unfocus(world)
            controls.pan_left = true
            return
        }
        if (e.key=='d'||e.key=='ArrowRight')
        {
            unfocus(world)
            controls.pan_right = true
            return
        }
        if (e.key=='w'||e.key=='ArrowUp')
        {
            unfocus(world)
            controls.pan_up = true
            return
        }
        if (e.key=='s'||e.key=='ArrowDown')
        {
            unfocus(world)
            controls.pan_down = true
            return
        }
        if (e.key=='Escape')
            return unfocus(world)
    })
    document.addEventListener('keyup', e=>{
        if (e.key=='a'||e.key=='ArrowLeft')
            return controls.pan_left = false
        if (e.key=='d'||e.key=='ArrowRight')
            return controls.pan_right = false
        if (e.key=='w'||e.key=='ArrowUp')
            return controls.pan_up = false
        if (e.key=='s'||e.key=='ArrowDown')
            return controls.pan_down = false
    })
    window.addEventListener('wheel', e=>{
        let delta = settings.camera_zoom_speed*e.deltaY
        camera.zoom = Math.max(0.25, Math.min(5, camera.zoom - delta))
    })
    window.addEventListener('click', e=>{
        let {camera} = world.context
        let mouse_x = e.clientX-camera.x
        let mouse_y = e.clientY-camera.y
        let entities = grid.lookup(mouse_x, mouse_y)
        let matches = []
        for (let e of entities)
        {
            let {x, y} = e.get(Thing.position)
            let distance = Math.hypot(x-mouse_x, y-mouse_y)
            if (distance <= settings.mouse_target_distance)
                matches.push(e)
        }
        let match = matches[0]
        if (match)
        {
            if (match.has(UI.focused))
            {
                match.remove(UI.tracking)
                unfocus(world)
            }
            else
            {
                match.add(UI.tracking)
                focus_thing(match, world)
            }
            event_bus.emit('ui.click')
        }
    })
    event_bus.on('tracking.remove', entity=>entity.remove(UI.tracking))
}

function focused_skew(world){
    let {viewport} = world.context
    let [focused] = world.query([UI.focused])
    if (focused)
    {
        let {x, y} = focused.get(Thing.position)
        return {x: -x+viewport.width/2, y: -y+viewport.height/2}
    }
    return {x: 0, y: 0}
}

let render = (ctx, {world, fps, grid})=>{
    let debug = world.context.debug
    let annotate = (text, x, y)=>{
        if (Array.isArray(text))
            text = text.join('\n')
        ctx.fillStyle = 'black'
        ctx.font = '10px sans'
        ctx.fillText(text, x, y)
    }
    ctx.fillStyle = 'white'
    ctx.fillRect(0, 0, ctx.canvas.width, ctx.canvas.height)
    let [focused] = world.query([UI.focused])
    if (focused)
        draw_target(ctx, 30, world.context.viewport)
    let {camera} = world.context
    ctx.translate(camera.x, camera.y)
    ctx.scale(camera.zoom, camera.zoom)
    if (debug)
        ctx.strokeRect(0, 0, ctx.canvas.width, ctx.canvas.height)
    for (let grass of world.query([Grass.type]))
    {
        let {x, y} = grass.get(Thing.position)
        let energy = grass.get(Thing.energy)
        let radius = draw_grass(ctx, grass, x, y)
        if (debug)
            annotate(`${energy.toFixed(1)}J`, x+radius+5, y)
    }
    for (let beep of world.query([Beep.type]))
    {
        let {x, y} = beep.get(Thing.position)
        draw_beep(ctx, beep, x, y)
        if (beep.has(UI.show_name))
        {
            let name = beep.get(Beep.name)
            annotate(name, x+10, y+3)
        }
        let target = beep.get(Beep.wander_to)
        if (debug && target)
        {
            ctx.strokeStyle = 'pink'
            ctx.beginPath()
            ctx.moveTo(x, y)
            ctx.lineTo(target.x, target.y)
            ctx.closePath()
            ctx.stroke()
        }
        if (debug)
        {
            let size = beep.get(Beep.size)
            let energy = beep.get(Thing.energy)
            ctx.beginPath()
            ctx.arc(x, y, Math.hypot(size/2, size/2), 0, 2*Math.PI)
            ctx.closePath()
            ctx.lineWidth = 1
            ctx.strokeStyle = 'rgba(255, 0, 0, 0.5)'
            ctx.stroke()
            annotate([`${energy.toFixed(1)}J`], x+size/2+5, y)
        }
    }
    for (let cloud of world.query([Cloud.type]))
    {
        let {size} = cloud.get(Cloud.type)
        let {x, y} = cloud.get(Thing.position)
        ctx.beginPath()
        ctx.fillStyle = 'rgba(0, 0, 0, 0.05)'
        ctx.arc(x, y, size, 0, 2*PI)
        ctx.fill()
        ctx.closePath()
    }
    if (debug)
    {
        ctx.strokeStyle = 'rgba(0, 255, 0, 0.25)'
        for (let [box] of grid.cells())
            ctx.strokeRect(box.x, box.y, box.x2-box.x, box.y2-box.y)
    }
    ctx.setTransform(1, 0, 0, 1, 0, 0)
    if (debug)
    {
        ctx.fillStyle = 'rgba(0, 0, 0, 0.4)'
        ctx.fillRect(0, 0, 70, 30)
        let fps_str = fps.v.toFixed(0).padStart(2, '0') + ' FPS'
        ctx.fillStyle = 'white'
        ctx.font = '20px serif'
        ctx.fillText(fps_str, 5, 20)
    }
}

let unit_vector = ({x, y})=>{
    let magnitude = Math.sqrt(x**2 + y**2)
    return {x: x/magnitude, y: y/magnitude}
}

let apply_systems = context=>{
    for (let fn of SYSTEMS)
        fn(context)
}

function drop_from_grid({world, grid}){
    let removed = [...Thing.position.removed()]
    for (let [entity, {x, y}] of removed)
    {
        let grid_key = grid.key(x, y)
        entity.event(`[${entity.id}] rm from grid x=${x} y=${y} (${grid_key})`)
        grid.delete(x, y, entity)
    }
    world.flush()
}

function* range(n){
    for (let i = 0; i<n; i++)
        yield i
}
