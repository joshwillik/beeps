import Thing from './type/Thing.mjs'
import Cloud from './type/Cloud.mjs'
import Wind from './type/Wind.mjs'

export function wind_on_clouds({world}){
    let wind = world.query([Wind.force])[0]
    let {x: dx, y: dy} = wind.get(Wind.force)
    for (let e of world.query([Cloud.type]))
    {
        let {z} = e.get(Cloud.type)
        let dz = Math.log(Math.max(z, 1.5))
        e.add(Thing.movement, {x: dx*dz, y: dy*dz})
    }
}

export function clouds_despawn({world}){
    let {stage} = world.context
    for (let e of world.query([Cloud.type]))
    {
        let {size} = e.get(Cloud.type)
        let {x, y} = e.get(Thing.position)
        if (!overlaps(circle_box(x, y, size), stage.bounding_box()))
            world.remove(e)
    }
}

function circle_box(x, y, size){
    return {
        x: x-size,
        x2: x+size,
        y: y-size,
        y2: y+size,
    }
}


function overlaps(a, b){
    return !(a.x>b.x2 || a.x2<b.x) // a is not out of b's range in x
        && !(a.y > b.y2 || a.y2<b.y) // a is not out of b's range in y
}
