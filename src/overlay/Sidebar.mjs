import {createElement as el, useContext, useRef, useEffect, useState} from 'react'
import Event_bus_context from './Event_bus_context.mjs'
import {draw_beep, draw_grass, draw_dead, draw_unknown, draw_target} from '../render.mjs'
import UI, {focus_thing} from '../type/UI.mjs'
import Beep from '../type/Beep.mjs'
import Grass from '../type/Grass.mjs'
import {ENTITY} from '../ecs.mjs'
import World_context from './World_context.mjs'

function useRender(fn, deps){
    let event_bus = useContext(Event_bus_context)
    useEffect(()=>{
        fn()
        event_bus.on('canvas.render', fn)
        return ()=>event_bus.off('canvas.render', fn)
    }, [event_bus, ...deps])
}

function Sidebar({entities}){
    if (!entities?.length)
        return null
    return el('div', {
        style: {
            position: 'absolute',
            zIndex: 10,
            top: 0,
            bottom: 0,
            right: 0,
            width: '400px',
            background: 'white',
            maxHeight: '100vh',
            overflow: 'auto',
            border: '1px solid black',
        },
    }, entities.map(e=>el(Sidebar_entity, {key: e.id, entity: e})))
}

function Sidebar_entity({entity}){
    let event_bus = useContext(Event_bus_context)
    let canvas_ref = useRef()
    useRender(()=>{
        if (!canvas_ref.current)
            return
        let ctx = canvas_ref.current.getContext('2d', {alpha: false})
        ctx.fillStyle = 'white'
        ctx.fillRect(0, 0, 100, 100)
        if (entity.DEAD)
            draw_dead(ctx, 100/2-20, 100/2)
        else if (entity.has(Beep.type))
            draw_beep(ctx, entity, 100/2, 100/2)
        else if (entity.has(Grass.type))
            draw_grass(ctx, entity, 100/2, 100/2)
        else
            draw_unknown(ctx, 100/2, 100/2)
        if (entity.has(UI.focused))
            draw_target(ctx, 30, {width: 100, height: 100})
    }, [canvas_ref, entity])
    return el('div', {className: 'thing-preview'}, [
        el('div', {key: 'preview'},
            el('canvas', {
                key: 'canvas',
                width: 100,
                height: 100,
                ref: canvas_ref,
                onClick: ()=>focus_thing(entity, world),
            }),
        ),
        el(Entity_stats, {entity}),
        el('div', {
            key: 'remove',
            style: {
                position: 'absolute',
                top: '10px',
                right: '10px',
                color: 'red',
                cursor: 'pointer',
                fontFamily: 'sans',
                fontWeight: 'bold',
            },
            onClick(event){
                event.preventDefault()
                event_bus.emit('tracking.remove', entity)
                event_bus.emit('ui.click', entity)
            },
        }, 'X'),
    ])
}

function Entity_stats({entity}){
    let [stats, set_stats] = useState(()=>[])
    useRender(()=>{
        let _stats = [...entity.components]
            .sort((a, b)=>a.name<b.name ? -1 : 1)
            .map(component=>{
                let v = entity.get(component)
                let name = component.name
                if (v===undefined)
                    return [name]
                if (component.draw)
                    return [name, component.draw(v)]
                return [name, v]
            })
        set_stats(_stats)
    }, [entity])
    return el('div', {
        key: 'stats',
        style: {margin: '8px'},
    }, [['id', entity.id], ...stats].map(([k, v])=>{
        if (v==undefined)
            return el('div', {key: k}, k)
        return el('div', {key: k}, k, ': ', el(Stat_value, {value: v}))
    }))
}

function Stat_value({value}){
    if (value.type==ENTITY)
        return el(Entity_icon, {entity: value})
    if (typeof value=='object')
        return JSON.stringify(value)
    return value
}

function Entity_icon({entity}){
    let world = useContext(World_context)
    let label = 'Entity'
    let color = '#333'
    if (entity.has(Beep.type))
    {
        label = 'Beep'
        color = 'blue'
    }
    if (entity.has(Grass.type))
    {
        label = 'Grass'
        color = 'green'
    }
    return el('span', {
        className: 'thing-icon',
        style: {color},
        onClick: ()=>entity.add(UI.tracking),
    }, `<${label} id=${entity.id}/>`)
}

export default Sidebar
