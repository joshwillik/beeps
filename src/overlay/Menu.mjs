import {createElement as el, useContext, useState} from 'react'
import World_context from './World_context.mjs'
import skill_tree_img from '../img/skill_tree.png'
import './Menu.css'

let buttons = [
    {id: 'skill_tree', label: 'Genetics', icon: skill_tree_img},
]

export default function Menu(){
    return el('div',
        {className: 'menu-bar'},
        buttons.map(b=>el(Menu_button, {key: b.id, button: b}))
    )
}

function Menu_button({button: {label, icon}}){
    let [show_label, set_show_label] = useState(false)
    return el(
        'div',
        {
            className: 'menu-item',
            onMouseEnter: ()=>set_show_label(true),
            onMouseLeave: ()=>set_show_label(false),
        },
        el('div', {className: 'icon'}, el('img', {src: icon})),
        show_label && el('div', {className: 'label'}, label),
    )
}
