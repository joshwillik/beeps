import {createElement as el, useState, useEffect, useContext, useMemo} from 'react'
import classnames from 'classnames'
import {sleep} from '../util.mjs'
import Beep from '../type/Beep.mjs'
import UI from '../type/UI.mjs'
import {useKeydown} from '../input.mjs'
import World_context from './World_context.mjs'

export default function Tour(){
    let [state_i, set_state_i] = useState(1)
    let on_next = ()=>{
        set_state_i(state_i+1)
    }
    let states = [
        Start,
        Game_intro,
        ()=>null, // just something to hide the overlay
    ]
    return el(
        'div',
        {className: 'tour'},
        el(states[state_i], {onNext: on_next}),
    )
}

function Start({onNext}){
    let [started, set_started] = useState(false)
    return el(
        'div', 
        {
            className: classnames('tour-start', started && 'tour-start-faded'),
            onTransitionEnd: onNext,
        },
        el('h1', {}, 'Beeps'),
        el('button', {onClick: ()=>set_started(true)}, 'Begin'),
    )
}

function Game_intro({onNext}){
    let world = useContext(World_context)
    let beep = useMemo(()=>world.query([Beep.type])[0], [])
    let name = beep.get(Beep.name)
    let messages = [
        ['Hi! Welcome to the wonderful world of beeps'],
        // TODO josh: make this sound more fun
        // TODO josh: read the real beep name
        [`Meet your first beep, ${name}`, el(Introduce_first_beep, {beep})],
        [`${name.split(' ')[0]} looks a little hungy, let's teach him how to farm`],
    ]
    let [message_i, set_message_i] = useState(2)
    let on_next = ()=>{
        if (message_i<messages.length-1)
            set_message_i(v=>v+1)
        else
            onNext()
    }
    let [message, comp] = messages[message_i]
    return el(
        'div', 
        {className: 'tour-messages'},
        el(Typed_message, {message, onNext: on_next}),
        !!comp && comp,
    )
}

function Introduce_first_beep({beep}){
    useEffect(()=>{
        beep.add(UI.show_name)
        return ()=>{
            beep.remove(UI.show_name)
        }
    }, [beep])
}

function Typed_message({message, onNext}){
    message = message||'????'
    let [length, set_length] = useState(0)
    useKeydown('Enter', ()=>{
        if (length>=message.length)
            onNext()
        else
            set_length(message.length)
    }, [length, set_length, message, onNext])
    useEffect(()=>set_length(0), [message])
    useEffect(()=>{
        if (length>=message.length)
            return
        let wait = setTimeout(()=>{
            set_length(length+1)
        }, 50)
        return ()=>clearTimeout(wait)
    }, [message, length])
    return el(
        'p',
        {},
        message.slice(0, length),
        el('small', {
            style: {
                opacity: length>=message.length ? 1 : 0,
            },
        }, '(⏎ to continue)')
    )
}
