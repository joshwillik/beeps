import {createElement as el } from 'react'
import World_context from './World_context.mjs'
import Sidebar from './Sidebar.mjs'
import Menu from './Menu.mjs'
import Tour from './Tour.mjs'

export default function Overlay(){
    return el('div', {}, el(Tour), el(Sidebar), el(Menu))
}
