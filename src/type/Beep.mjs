import {assert, random} from '../util.mjs'
import {component} from '../ecs.mjs'
import Thing from './Thing.mjs'
import Grass from './Grass.mjs'
import UI from './UI.mjs'
import {angle_between, distance_between, vector_parts} from '../trig.mjs'
import {build_name} from '../names.mjs'

let fps = 60
let percent = v=>`${(v*100).toFixed(2)}%`
let percent_sec = v=>`${(v*fps*100).toFixed(2)}%/sec`
let assert_coords = ({x, y})=>{
    assert(!Number.isNaN(x), 'x is NaN')
    assert(!Number.isNaN(y), 'y is NaN')
}
let Beep = {
    type: component({name: 'beep'}),
    name: component({name: 'beep.name'}),
    goal: component({name: 'beep.goal'}),
    hunting: component({
        name: 'beep.hunting',
    }),
    fleeing: component({
        name: 'beep.fleeing',
    }),
    wander_to: component({
        name: 'beep.wander_to',
        validate: assert_coords,
    }),
    shield: component({
        name: 'beep.shield',
        draw(v){ return v.toFixed(3) },
        default: 0,
        validate({size}){
            assert(!Number.isNaN(size), 'size is NaN')
        },
    }),
    decisiveness: component({
        name: 'beep.decisiveness',
    }),
    size: component({
        name: 'beep.size',
        draw(v){ return v.toFixed(3) },
        validate({size}){
            assert(!Number.isNaN(size), 'size is NaN')
        },
    }),
    min_size: component({
        name: 'beep.min_size',
        draw(v){ return v.toFixed(3) },
    }),
    max_size: component({
        name: 'beep.max_size',
        draw(v){ return v.toFixed(3) },
    }),
    energy_reserves: component({
        name: 'beep.energy_reserves',
        draw(v){ return v.toFixed(3) },
    }),
    growth_rate: component({
        name: 'beep.growth_rate',
        draw: percent_sec,
    }),
    growth_loss: component({
        name: 'beep.growth_loss',
        draw: percent,
    }),
    reclaim_rate: component({
        name: 'beep.reclaim_rate',
        draw: percent_sec,
    }),
    reclaim_loss: component({
        name: 'beep.reclaim_loss',
        draw: percent,
    }),
    shield_rate: component({
        name: 'beep.shield_rate',
        draw: percent_sec,
    }),
    vision: component({
        name: 'beep.vision',
        draw(v){ return v.toFixed(3) },
    }),
    decision: component({name: 'beep.decision_age'}),
    can_see: component({
        name: 'beep.can_see',
        draw(list){ return [...list].map(x=>x.id).join(', ') },
    }),
    can_reach: component({
        name: 'beep.can_reach',
        draw(list){ return [...list].map(x=>x.id).join(', ') },
    }),
}

export function beep_vision(beep){
    let m = beep.get(Thing.movement)||{x: 0, y: 0}
    let angle = Math.atan2(m.y, m.x)
    let angle_left = angle-Math.PI/4
    let angle_right = angle+Math.PI/4
    return {
        angle_left,
        angle_right,
        distance: beep.get(Beep.size)*beep.get(Beep.vision),
    }
}

export function beep_can_reach(beep, target_pos){
    let size = beep.get(Beep.size)
    let {x, y} = beep.get(Thing.position)
    let {x: x2, y: y2} = target_pos
    let reach = Math.hypot(size, size)
    let distance = Math.hypot(x-x2, y-y2)
    return reach>=distance
}

export function beep_can_see(a, b){
    let vision = beep_vision(a)
    let a_position = a.get(Thing.position)
    let b_position = b.get(Thing.position)
    let ray = {
        x: b_position.x-a_position.x,
        y: b_position.y-a_position.y,
    }
    if (Math.hypot(ray.x, ray.y)>vision.distance)
        return false
    let ray_angle = Math.atan2(ray.y, ray.x)
    return angle_between(vision.angle_left, ray_angle, vision.angle_right)
}

export function add_beep(world, override={}){
    let {stage} = world.context
    let x = override.x ?? random(0, stage.x)
    let y = override.y ?? random(0, stage.y)
    let beep = world.entity()
        .add(Beep.type)
        .add(Beep.name, build_name())
        .add(Thing.energy, override.energy || random(0, 100))
        .add(Thing.position, {x, y})
        .add(Beep.size, random(5, 20))
        .add(Beep.min_size, random(2, 5))
        .add(Beep.max_size, random(30, 300))
        .add(Beep.energy_reserves, random(10, 2000))
        .add(Beep.growth_rate, random(0.01/fps, 0.1/fps))
        .add(Beep.growth_loss, random(0.01, 0.5))
        .add(Beep.reclaim_rate, random(0.01/fps, 0.1/fps))
        .add(Beep.reclaim_loss, random(0.01, 0.5))
        .add(Beep.goal, 'idle')
        .add(Beep.shield_rate, random(0.01, 0.5))
        .add(Beep.decisiveness, Math.floor(random(20, 80)))
        .add(Beep.vision, random(1.1, 3))
    return beep
}

export function pick_scary(beep){
    let size = beep.get(Beep.size)
    for (let other of beep.get(Beep.can_see))
    {
        if (!other.has(Beep.type))
            continue
        if (0 && other.get(Beep.size)>size)
            return true
    }
}

// TODO josh: it looks like this function is behaving wrong.
// It's not, can_see calculation is forgetting about things as soon as they
// cross into a new grid cell.
// It's probably time to switch to quad-trees or include sibling cells so this doesn't happen
export function pick_huntable(beep){
    let position = beep.get(Thing.position)
    let huntable = Array.from(beep.get(Beep.can_see))
    .filter(other=>!other.DEAD && would_hunt(beep, other))
    .map(other=>[other, distance_between(position, other.get(Thing.position))])
    .sort((a, b)=>a[1]-b[1])
    .map(x=>x[0])
    return huntable[0]
}

function would_hunt(beep, other){
    if (other.has(Grass.type))
        return true
    if (other.has(Beep.type) && other.get(Beep.size) <= beep.get(Beep.size)*0.9)
        return true
    return false
}

export function pick_wander(beep){
    let vision = beep_vision(beep)
    let wander = vector_parts(random(vision.angle_left, vision.angle_right), vision.distance)
    let {x, y} = beep.get(Thing.position)
    return {x: x+wander.x, y: y+wander.y}
}

export default Beep
