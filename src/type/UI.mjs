import {component} from '../ecs.mjs'

let UI = {
    tracking: component({name: 'ui.tracking'}),
    focused: component({name: 'ui.focused'}),
    show_name: component({name: 'ui.show_name'}),
}

export function focus_thing(thing, world){
    unfocus(world)
    thing.add(UI.focused)
}

export function unfocus(world){
    for (let e of world.query([UI.focused]))
        e.remove(UI.focused)
}

export default UI
