import {assert} from '../util.mjs'
import {component} from '../ecs.mjs'

let Thing = {
    position: component({
        name: 'position',
        validate({x, y}, old){
            assert(!Number.isNaN(x), 'x is NaN')
            assert(!Number.isNaN(y), 'y is NaN')
            if (old)
            {
                let dy = Math.abs(old.y-y)
                assert(dy<50, `Things can't move ${dx} pixels per frame`)
                let dx = Math.abs(old.x-x)
                assert(dx<50, `Things can't move ${dx} pixels per frame`)
            }
        },
        draw({x, y}){ return `x=${x.toFixed(2)} y=${y.toFixed(2)}` },
    }),
    movement: component({
        name: 'movement', 
        validate({x, y}){
            assert(!Number.isNaN(x), 'x is NaN')
            assert(!Number.isNaN(y), 'y is NaN')
        },
        draw({x, y}){ return `x=${x.toFixed(2)} y=${y.toFixed(2)}` },
        default(){ return {x: 0, y: 0} },
    }),
    energy: component({
        name: 'energy',
        draw(v){ return v.toFixed(3) },
    }),
}
export default Thing
