import {component} from '../ecs.mjs'

let Wind = {
    force: component({name: 'wind.force'})
}
export default Wind
