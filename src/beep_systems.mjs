import Thing from './type/Thing.mjs'
import Beep, {beep_can_reach, beep_can_see} from './type/Beep.mjs'
import Grass from './type/Grass.mjs'

class Set_map extends Map {
    get(k){
        if (!this.has(k))
            this.set(k, new Set())
        return super.get(k)
    }
}

export function beeps_see({grid}){
    let beep_visions = new Set_map()
    let beep_reaches = new Set_map()
    for (let [, things] of grid.cells())
    {
        things = [...things]
        for (let i = 0; i<things.length; i++)
        {
            let thing = things[i]
            let visible = beep_visions.get(thing)
            let reach = beep_reaches.get(thing)
            for (let ii = i+1; ii<things.length; ii++)
            {
                let other = things[ii]
                if (thing.has(Beep.type))
                {
                    if (beep_can_see(thing, other))
                        visible.add(other)
                    if (beep_can_reach(thing, other.get(Thing.position)))
                        reach.add(other)
                }
                if (other.has(Beep.type))
                {
                    if (beep_can_see(other, thing))
                        beep_visions.get(other).add(thing)
                    if (beep_can_reach(other, thing.get(Thing.position)))
                        beep_reaches.get(other).add(thing)
                }
            }
            if (thing.has(Beep.type))
            {
                thing.add(Beep.can_see, visible)
                thing.add(Beep.can_reach, reach)
            }
        }
    }
}

export function beeps_fight({world}){
    let dead = new Set()
    for (let beep of world.query([Beep.type]))
    {
        if (dead.has(beep))
            continue
        let hunting = beep.get(Beep.hunting)
        let can_reach = beep.get(Beep.can_reach)
        if (hunting && can_reach?.has(hunting))
        {
            beep.update(Thing.energy, v=>v + hunting.get(Thing.energy)*0.6)
            dead.add(hunting)
        }
    }
    for (let e of dead)
        world.remove(e)
}

export function beeps_starve({world}){
    for (let e of world.query([Beep.type]))
    {
        if (e.get(Thing.energy)<=0)
            world.remove(e)
    }
}

export function beeps_bounce_off_walls({world}){
    let {stage} = world.context
    for (let e of world.query([Beep.type]))
    {
        let {x, y} = e.get(Thing.position)
        let {x: dx, y: dy} = e.get(Thing.movement)
        if (y>stage.y)
            dy = Math.abs(dy)*-1
        if (y<=0)
            dy = Math.abs(dy)
        if (x>stage.x)
            dx = Math.abs(dx)*-1
        if (x<=0)
            dx = Math.abs(dx)
        e.add(Thing.movement, {x: dx, y: dy})
    }
}

export function beeps_shield({world, dt}){
    for (let e of world.query([Beep.type]))
    {
        let shield = e.get(Beep.shield)
        let shield_rate = e.get(Beep.shield_rate)
        if (e.get(Beep.goal)!='shield')
            continue
        let shield_increase = Math.min(1*shield_rate*dt, 100-shield)
        if (shield_increase<=0)
            continue
        e.update(Beep.shield, s=>Math.min(s+shield_increase, 100))
        e.update(Thing.energy, energy=>energy-shield_increase)
    }
}

let area = r=>2*Math.PI*r
export function beeps_grow({world, dt}){
    for (let e of world.query([Beep.type]))
    {
        let spare_energy = e.get(Thing.energy)-e.get(Beep.energy_reserves)
        let size_delta = 0
        let energy_delta = 0
        let size = e.get(Beep.size)
        if (spare_energy>0)
        {
            let growth_left = e.get(Beep.max_size)-size
            size_delta = growth_left * e.get(Beep.growth_rate) * dt
            energy_delta = (area(size) - area(size+size_delta)) * (1+e.get(Beep.growth_loss))
        }
        else
        {
            let allowed_shrink = e.get(Beep.min_size)-size
            size_delta = allowed_shrink * e.get(Beep.reclaim_rate) * dt
            energy_delta = (area(size) - area(size+size_delta)) * (1-e.get(Beep.reclaim_loss))
        }
        e.update(Beep.size, s=>s+size_delta)
        e.update(Thing.energy, energy=>energy+energy_delta)
    }
}
