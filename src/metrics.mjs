import {assert} from './util.mjs'

export class FPS {
    constructor(opt){
        this.smoothing = opt?.smoothing || 0.9
        this.v = 0
        this.ts = 0
    }
    inc(){
        let ts = Date.now()
        let frame_time = ts-this.ts
        // TODO lou: figure out why this is wrong at a root level
        // (calling more than once per frame)
        if(frame_time == 0)
            frame_time = 1
        assert(!Number(frame_time<=0), 'frame_time is <=0')
        let fps = 1000/frame_time
        let smoothing = Math.pow(this.smoothing, frame_time * 60 / 1000)
        this.v = this.v*smoothing + fps*(1-smoothing)
        this.ts = ts
    }
}
