// TODO josh: this math is wrong (it doesn't handle cases where the visibility
// spotlight crosses over the -PI or +PI.
// But I'm bored of doing trig so it's correct enough for now.
// Also, fun fact: GPT wrote most of this
export function angle_between(left_edge, angle, right_edge){
    if (right_edge < left_edge)
        return angle >= left_edge || angle <= right_edge
    else
        return angle >= left_edge && angle <= right_edge
}

export function distance_between(a, b){
    return Math.hypot(a.x-b.x, a.y-b.y)
}

export function vector_parts(angle, scale){
    let y = Math.sin(angle)
    let x = Math.cos(angle)
    return {x: x*scale, y: y*scale}
}
