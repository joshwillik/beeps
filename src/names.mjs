import {shuffle} from 'lodash-es'

let base_names = [
    'Beepo Niskanen',
    'Arbeep Amano',
    'Jubeep Kwon',
    'Albeep Singh',
    'Beepan Balakrishnan',
    'Beepal Patel',
    'Najbeepa Al-Shehri',
    'Beepah Zaman',
    'Mabeepa Asad',
    'Beepashree Desai',
    'Beepasak Pariyar',
    'Kibeepa Sato',
    'Beepashvili Khutsishvili',
    'Chubeepa Nguyen',
    'Beepah Khan',
    'Beependra Chauhan',
    'Beepanee Mathur',
    'Nabeepa Arora',
    'Gibeepa Kim',
    'Beepoo Chatterjee',
    'Beepalota Singh',
    'Aibeepa Katoch',
    'Beepreet Kaur',
    'Kabeepa Okafor',
    'Beepu Chakraborty',
    'Beepaulin Ong',
    'Tabeepa Usman',
    'Beepul Roy',
    'Beepal Shrestha',
    'Beependra Pradhan',
    'Beepa Rahman',
    'Beepenzi Njoroge',
    'Wubeepa Haile',
    'Beepusri Vithanage',
    'Beepushree Mukherjee',
    'Beepulakshmi Sharma',
    'Beepalakrishna Bhat',
    'Sabeepa Ahmed',
    'Beepalakshmy Nair',
    'Beepishetty Reddy',
    'Beepali Bhandari',
    'Beepal Vashisth',
    'Beepin Yadav',
    'Beepika Singh',
    'Beepin Nair',
    'Beepali Goyal',
    'Beepal Mahajan',
    'Beepindar Singh',
    'Beepankar Ray',
    'Beepasri Banerjee',
    'Beepster Smith',
    'Beepamela Anderson',
    'Beepett Johansson',
    'Beepward Hughes',
    'Beepatrick Stewart',
    'Beepeth Paltrow',
    'Beepam Sandler',
    'Beepatricia Arquette',
    'Beepan Affleck',
    'Beepan Bale',
    'Beepan Foster',
    'Beepan Reynolds',
    'Beepan Driver',
    'Beepan Theroux',
    'Beepan Wilson',
    'Beepan Willis',
    'Beepan Murray',
    'Beepan Segal',
    'Beepan Stiller',
    'Beepan Savage',
    'Jacob Beepman',
    'Samantha Beepster',
    'Lucas Beepsen',
    'Olivia Beepstein',
    'Ethan Beepworth',
    'Emma Beepman',
    'Tyler Beepson',
    'Ava Beepman',
    'Benjamin Beepley',
    'Madison Beepford',
    'William Beepington',
    'Isabella Beepman',
    'Daniel Beepson',
    'Sophia Beepman',
    'Logan Beepsley',
    'Mia Beepman',
    'Michael Beeperson',
    'Amelia Beepman',
    'Joshua Beepford',
    'Charlotte Beepman',
]

let choice_fn = choices=>{
    let i = 0
    return ()=>{
        if (i==0)
        {
            choices = shuffle(choices)
            i = choices.length
        }
        i--
        return choices[i]
    }
}

let first_name = choice_fn(base_names.map(n=>n.split(' ')[0]))
let last_name = choice_fn(base_names.map(n=>n.split(' ').pop()))

export function build_name(){
    return [
        first_name(),
        last_name(),
    ].join(' ')
}
