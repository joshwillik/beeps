import {useEffect} from 'react'

export function useKeydown(key, fn, deps){
    useEffect(()=>{
        let handler = e=>{
            if (e.key!=key)
                return
            e.preventDefault()
            fn()
        }
        document.addEventListener('keydown', handler)
        return ()=>document.removeEventListener('keydown', handler)
    }, [key, fn, ...deps])
}
