import {noop} from 'lodash-es'
import {assert} from './util.mjs'
export let ENTITY = Symbol('ecs.entity')
export let COMPONENT = Symbol('ecs.component')

// Library design
// ==============
// The goal of an ECS system is to allow you to create/remove components
// (properties) from entities.
// An entity can have any combination of properties, and each section of the
// game logic will only operate on the entities that have the right combination of components.
// E.g. Only things that have a position and direction of movement will move each game tick.
//
// Each entity holds a list of components and each component holds a list of entities.
// This operates something like a DB index, so you can easily batch select all
// the entities with a specific combination of properties.
//
// Code API
// --------
// // context is like "global" stuff that the whole game world needs to know about.
// // The "world" is the container for all the entities and components in the game
// let world = ecs.world(context)
// // A component is an entity attribute
// let position = world.component()
// let movement = world.component()
// let player = world.component()
// let focused = world.component()
// // world.add create adds a new entity to the game
// let entity = world.add()
// entity.add(position, {x: 10, y: 20})
// entity.add(movement, {x: 10, y: 20})
// entity.add(player)
// // When something new happens
// entity.add(focused)
// entity.remove(focused)
// // To find all the focused players
// let entities = world.query([player, focused])
//
// Design decisions
// ----------------
// Some ECS systems use string keys to reference components.
// I decided to use explicit object references to make it impossible to
// accidentally use the same from 2 places.
// If you don't have an explicit reference to the right component, you can't use it.

export function world(context = {}){
    let _entity_counter = 1
    let _entities = new Map()
    let entity_cleanup = new WeakMap()
    let seen_components = new Set()

    function entity(){
        let id = _entity_counter++
        let valid_component = c=>assert(c?.type==COMPONENT,
            'Passed component isn\'t an instance of world.component()')
        let components = new Set()
        let log = []
        let e = {
            type: ENTITY,
            id,
            components,
            log,
            event(message){ log.push(message) },
            add(component, data){
                valid_component(component)
                seen_components.add(component)
                component.set(e, data)
                components.add(component)
                return e
            },
            get(component){
                valid_component(component)
                return component.get(e)
            },
            // there's no set() function, use add
            update(component, data){
                valid_component(component)
                seen_components.add(component)
                return component.update(e, data)
            },
            has(component){
                valid_component(component)
                return component.has(e)
            },
            remove(component){
                valid_component(component)
                component.delete(e)
                components.delete(component)
            },
        }
        _entities.set(id, e)
        entity_cleanup.set(e, ()=>{
            e.DEAD = true
            e.event('rip')
            for (let c of components)
                c.delete(e)
            _entities.delete(id)
        })
        return e
    }

    function remove(entity){ entity_cleanup.get(entity)?.() }

    function query(q, initial_matches){
        // TODO josh: passing initial matches here is wrong, rethink this later
        if (!initial_matches)
        {
            initial_matches = q[0].all()
            q = q.slice(1)
        }
        let matches = []
        entity_loop:
        for (let id of initial_matches)
        {
            for (let c of q)
            {
                if(!c.has(id))
                    continue entity_loop
            }
            matches.push(id)
        }
        return matches
    }

    function flush(){
        for (let c of seen_components)
            c._flush()
    }

    return {
        context, // storage for the things that don't fit cleanly in the ECS system
        entity, // adding something to the world
        remove, // deleting something from the world
        flush, // flush deleted values out of the component delete queue
        query, // find entities by some combination of components
    }
}

export function component(opt){
    let values = new Map()
    let removed = new Map()
    let validate = opt?.validate || noop
    let on_delete = opt?.on_delete || noop
    let get_default = typeof opt.default=='function'
        ? opt.default
        : ()=>opt.default
    let c = {
        type: COMPONENT,
        name: opt.name,
        draw: opt.draw,
        get(e){ return values.get(e) },
        has(e){ return values.has(e) },
        set(e, v){
            validate(v)
            return values.set(e, v)
        },
        update(e, fn){
            let new_v = fn(values.has(e) ? values.get(e) : get_default())
            validate(new_v)
            values.set(e, new_v)
            return new_v
        },
        delete(e){
            let value = values.get(e)
            on_delete(e, value)
            removed.set(e, value)
            values.delete(e)
        },
        removed(){ return removed.entries() },
        all: values.keys.bind(values),
        _flush(){ removed.clear() },
        _values: values,
    }
    return c
}

export default {world, component}
