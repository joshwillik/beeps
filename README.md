# Beeps game

A smiulation/evolution game about little beeps that just want to live their life

## Game design

https://lore.kernel.org/git/xmqq5za8hpir.fsf@gitster.c.googlers.com/

## Installation instructions

```
$ npm install -D
$ ./run-local.sh
```

Then open src/index.js and get coding
